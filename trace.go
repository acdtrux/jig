package jig

import (
	"fmt"
	"path"
	"runtime"
)

// Trace/Tracer provides a flexible function that reduces thought processes in
// tracing the whereabouts of your business logic. You can call it with
// no args, with one string arg, or with a string message format and args
//	tracer := Tracer("hunt and peck")
//	tracer()
//	tracer("hey")
//	tracer("Hey now %s", "hey now now")

func (j *jig) Trace(info ...interface{}) {
	msg, extra := infoToFmt(info...)
	j.write("Trace", msg, extra...)
}

func (j *jig) Tracer(msg string, extra ...interface{}) func(...interface{}) {
	tracerInstancePreface := fmt.Sprintf("Tracer:%s", fmt.Sprintf(msg, extra...))
	return func(info ...interface{}) {
		msg, extra := infoToFmt(info...)
		j.write(tracerInstancePreface, msg, extra...)
	}
}

func traceCaller(skip int) string {
	_, file, no, ok := runtime.Caller(skip)
	if ok {
		return fmt.Sprintf("%s:%d", path.Base(file), no)
	}
	return "<unknown>"
}

func infoToFmt(info ...interface{}) (msg string, extra []interface{}) {
	if len(info) > 0 {
		var ok bool
		if msg, ok = info[0].(string); ok {
			extra = info[1:]
		} else {
			// msg will be blank, and extra will just show
			// as a bunch of dangling stuff in Sprintf later.
			extra = info
		}
	}
	return
}
