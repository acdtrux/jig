Jig - A Mindset for Wish Driven Development
===========================================

# Overview

Jig doesn't do anything special. It is plainly simple placeholder and tracing
debug features to allow developers to punt problems that can be solved in a
later effort, to encourage complete focus on one single problem at a time.

*A successful project has removed Jig from all of its imports.*

## Principles and Methodology

### Specify concepts in wireframes and shims

Develop your concept before you implement anything. You can write an entire
program that is a representation of your idealist world, saving all the
dangerous details for when you can focus _on them_.

### Implement solutions with scaffolding and trust

Flowing forward from your conceptual wireframe, you can drill in on solving
one problem at a time, assuming the whole rest of your world will support you.

### Focus now, defer tangents

Trust Jig to remind you of faked results and placeholder dependencies, so that
you can put them out of your mind while you focus on the specific problem front
and center.

### Continuous brainstorming

Just add "Ride a flying narwhal" to your wishlist at any point in your code.
No design spike, no issue ticket. When you realize that's a ridiculous idea, you
can delete it guilt-free.

### Stay accountable to ideas

Issue tickets and wikis are not always great for holding implementation details
and half-baked ideas. Moreover, referencing code in a writeup can be impossible
to keep synchronized, especially for a young project that gets refactored.

Use the Jig tools to scour and analyze your code for outstanding efforts, and
burden your project management only with reliable high-level information.

### Don't air your laundry in an MVP

Your work is never done. Just silence Jig outputs at runtime.


# Who uses Jig?

Probably only me, and that's just fine.

