package jig

import "fmt"

func Printfln(msg string, extra ...interface{}) {
	fmt.Println(fmt.Sprintf(msg, extra...))
}
