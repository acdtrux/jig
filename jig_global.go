package jig

// A default convenience global, because honestly, a tunable thing
// will not be useful to most users.
var j = New(WithStderr, WithEnable)
var (
	AddWriterHandler = j.AddWriterHandler
	Bail             = j.Bail
	Disable          = j.Disable
	Enable           = j.Enable
	Fixme            = j.Fixme
	Hazard           = j.Hazard
	NotImplemented   = j.NotImplemented
	Todo             = j.Todo
	Trace            = j.Trace
	Tracer           = j.Tracer
	Wishlist         = j.Wishlist
	Wonder           = j.Wonder
)
