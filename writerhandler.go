package jig

import (
	"fmt"
	"io"
)

type (
	writerHandler struct{ writer io.Writer }
)

var _ handler = &writerHandler{}

func (w *writerHandler) handle(jigname, preface, callersrc, format string, extra ...interface{}) {
	fmt.Fprintf(w.writer, "[%s:%s:%s] %s\n", jigname, preface, callersrc, fmt.Sprintf(format, extra...))
}
