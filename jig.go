package jig

// The jig package is a jig/scaffold placeholder meant to help the developer
// keep track of what they meant to be accountable for.
// When any project is successful, this package gets deleted.

import (
	"fmt"
	"io"
	"net"
	"os"
)

type (
	Jig interface {
		// Configuration / Setup
		Disable()
		Enable()
		AddWriterHandler(w io.Writer)
		/* TODO:

		   // Some more interactive features, especially handy for
		   // weird semi-manual testing apparatus
		   AddChanHandler (Adds a chan<- string to handlers)
		   AddFileHandler (Adds a handler that appends to a file)
		   AddNetListener (Adds a handler and sets up a socket server that sends all messages to all live clients)
		   AddNetClient (Adds a handler that insistently delivers messages to some address)

		   // Tunable laundry airing
		   SetLevel (ie, 1={Todo,Fixme}, 2=1+{Hazard,NotImplemented}, 3=2+{Wonder,Wishlist})
		*/

		// Basic messaging
		Todo(msg string, extra ...interface{})
		Fixme(msg string, extra ...interface{})
		Wonder(msg string, extra ...interface{})
		Wishlist(msg string, extra ...interface{})

		// Advanced messaging
		Hazard(category category, msg string, extra ...interface{})
		NotImplemented(msg string, extra ...interface{}) error

		// Live debugging
		// Trace functions `Trace(...)` and `Tracer()(...)` are designed
		// to give you a flexible statement so you don't have to think
		// much about your message format (in fact it can be omitted, or
		// even be just junk instead of a format)
		Trace(info ...interface{})
		Tracer(msg string, extra ...interface{}) func(...interface{})

		// os.Exit!
		Bail(exitCode int, msg string, extra ...interface{})
	}
	jig struct {
		name     string
		silence  bool
		handlers []handler
	}
	handler interface {
		handle(name, preface, callersrc, format string, extra ...interface{})
	}
	category string
	option   func(*jig)
)

const (
	Security category = "Security"
	Safety   category = "Safety"
)

var (
	WithStderr option = func(j *jig) { j.AddWriterHandler(os.Stderr) }
	WithEnable option = func(j *jig) { j.Enable() }
)

func WithName(name string) option {
	return func(j *jig) {
		j.name = name
	}
}

func WithWriterHandler(w io.Writer) option {
	return func(j *jig) {
		j.AddWriterHandler(w)
	}
}

func WithNetDial(proto string, addr string) option {
	return func(j *jig) {
		conn, err := net.Dial(proto, addr)
		if err != nil {
			panic(err)
		}
		j.AddWriterHandler(conn)
	}
}

func New(opts ...option) Jig {
	j := &jig{"jig", true, []handler{}}
	for _, opt := range opts {
		opt(j)
	}
	return j
}

func (j *jig) Disable() {
	j.silence = true
}

func (j *jig) Enable() {
	j.silence = false
}

func (j *jig) AddWriterHandler(w io.Writer) {
	j.handlers = append(j.handlers, &writerHandler{w})
}

func (j *jig) Todo(msg string, extra ...interface{}) {
	j.write("Todo", msg, extra...)
}

func (j *jig) Fixme(msg string, extra ...interface{}) {
	j.write("Fixme", msg, extra...)
}

func (j *jig) Wonder(msg string, extra ...interface{}) {
	j.write("Wonder", msg, extra...)
}

func (j *jig) Wishlist(msg string, extra ...interface{}) {
	j.write("Wishlist", msg, extra...)
}

func (j *jig) Hazard(category category, msg string, extra ...interface{}) {
	j.write(fmt.Sprintf("Hazard:%s", category), msg, extra...)
}

func (j *jig) NotImplemented(msg string, extra ...interface{}) error {
	j.write("NotImplemented", msg, extra...)
	return fmt.Errorf("NotImplemented(%s):%s", traceCaller(2), fmt.Sprintf(msg, extra...))
}

func (j *jig) Bail(exitCode int, msg string, extra ...interface{}) {
	fmt.Fprintln(os.Stderr, fmt.Sprintf(msg, extra...))
	os.Exit(exitCode)
}

func (j *jig) write(preface, msg string, extra ...interface{}) {
	if j.silence {
		return
	}
	for _, w := range j.handlers {
		w.handle(j.name, preface, traceCaller(3), msg, extra...)
	}
}
