package main

import (
	"bitbucket.org/acdtrux/jig"
)

func main() {
	demoPrintfln()
	demoConfigurableTracer()
	demoUDPTracer()
	demoBail()
}

func demoPrintfln() {
	jig.Printfln("You know you've always wanted %s", "this.")
}

func demoConfigurableTracer() {
	// Optionally create your own, or just enable the global one
	j := jig.New(
		jig.WithStderr,
		jig.WithEnable,
		jig.WithName("ExampleJig"),
	)

	tracer := j.Tracer("main()")
	tracer("starting")
	j.Todo("Implement a complicated system")
	j.Wishlist("Integrate with external systems")
	tracer()
	j.Hazard(jig.Security, "Must implement auth")
	tracer("finishing")
	j.Trace("trace")
	j.Trace()
	j.Trace(j)

}

func demoUDPTracer() {
	// (Alternate, verbose style, if you have some writer already)
	// conn, err := net.Dial("udp", "127.0.0.1:7199") // NOTE 7199 spells JIGG
	// if err != nil {
	//     panic(err)
	// }
	// // ...
	//     jig.WithWriterHandler(conn),
	// // ...

	j := jig.New(
		jig.WithStderr,
		jig.WithNetDial("udp", "127.0.0.1:7199"), // NOTE 7199 spells JIGG
		jig.WithEnable,
		jig.WithName("UDPJig"),
	)
	j.Todo("Hello UDP and STDERR at the same time!")
	j.Wishlist("To see this complete demo, use an equivalent command to `netcat -u -l 7199`")
	j.Trace("Hello!", j)
}

func demoBail() {
	j := jig.New(
		jig.WithStderr,
		jig.WithEnable,
		jig.WithName("ExampleJig"),
	)

	err := j.NotImplemented("not ready for prod")
	if err != nil {
		j.Bail(1, "Error in main: %s. (NOTE: This means the demo succeeded)", err)
	}
}
